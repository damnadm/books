<?php
/**
 * Created by PhpStorm.
 * User: serabalint
 * Date: 2018. 04. 01.
 * Time: 20:52
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\DocBlock\Description;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Book
 * @package App\Entity
 *
 * @ApiResource
 * @ORM\Entity
 */
class Book
{

    /**
     * @var The id of this book.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Assert\Isbn
     */
    private $id;

    /**
     * @var string|null The ISBN of the book
     *
     * @ORM\Column(nullable=true)
     * @Assert\NotBlank
     */
    public $isbn;

    /**
     * @var string title of the book
     *
     * @ORM\Column
     */
    public $title;

    /**
     * @var Description of the Book
     *
     * @ORM\Column
     */
    public $description;

    /**
     * @var The author of this book.
     *
     * @ORM\Column
     */
    public $author;

    /**
     * @var \DateTimeInterface The publication date of this book
     *
     * @ORM\Column(type="datetime")
     * @Assert\NotNull
     */
    public $publicationDate;

    /**
     * @var Review[] ArrayCollection Available reviews for this book.
     *
     * @ORM\OneToMany(targetEntity="Review", mappedBy="book")
     */
    public $reviews;

    /**
     * Book constructor.
     */
    public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

}